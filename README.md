# Round-Robin POC

This is a proof of concept for a round-robin load balancer service written in Go. It uses the Echo web framework and is structured to include HTTP handlers, HTTP clients, and use cases.
![img.png](img.png)
## Prerequisites

- Go 1.19 or later
- Git

## Installation

### Clone the Repository

First, clone the repository to your local machine:

```sh
git clone https://github.com/your-username/round-robin-poc.git
cd round-robin-poc
```

### Install Depdencies
First, clone the repository to your local machine:

```sh
make init
```

### Run
```sh
make run-api
make run-loadbalancer
```