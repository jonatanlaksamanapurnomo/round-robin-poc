package uc_echomessage

import (
	"context"
	"fmt"
	"github.com/golang/mock/gomock"
	"reflect"
	"server/simple-api/domain/echomessage"
	"testing"
)

func TestUsecase_DoEcho(t *testing.T) {
	ctrl := gomock.NewController(t)
	echoDomainMock := echomessage.NewMockDomainItf(ctrl)
	defer ctrl.Finish()

	type fields struct {
		echoDomain echomessage.DomainItf
	}
	defaultFields := fields{
		echoDomain: echoDomainMock,
	}
	defaultDataArgs := map[string]interface{}{
		"key": "value",
	}
	type args struct {
		ctx  context.Context
		data map[string]interface{}
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]interface{}
		wantErr bool
		mock    func()
	}{
		{
			name:   "Error DoEcho Domain Call",
			fields: defaultFields,
			args: args{
				data: defaultDataArgs,
			},
			want:    nil,
			wantErr: true,
			mock: func() {
				echoDomainMock.EXPECT().DoEcho(gomock.Any(), defaultDataArgs).Return(nil, fmt.Errorf("error"))
			},
		},
		{
			name:   "Success DoEcho Domain Call ",
			fields: defaultFields,
			args: args{
				data: defaultDataArgs,
			},
			want:    defaultDataArgs,
			wantErr: false,
			mock: func() {
				echoDomainMock.EXPECT().DoEcho(gomock.Any(), defaultDataArgs).Return(defaultDataArgs, nil)
			},
		},
	}
	for _, tt := range tests {
		tt.mock()
		t.Run(tt.name, func(t *testing.T) {
			u := &Usecase{
				echoDomain: tt.fields.echoDomain,
			}
			got, err := u.DoEcho(tt.args.ctx, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("DoEcho() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoEcho() got = %v, want %v", got, tt.want)
			}
		})
	}
}
