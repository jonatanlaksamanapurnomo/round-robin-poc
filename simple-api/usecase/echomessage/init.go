package uc_echomessage

import (
	"context"
	"server/simple-api/domain/echomessage"
)

//go:generate mockgen -source=init.go -destination=init_mock.go -package=uc_echomessage
type EchoUsecaseItf interface {
	DoEcho(ctx context.Context, data map[string]interface{}) (map[string]interface{}, error)
}

func InitUsecase(
	echoDomain echomessage.DomainItf,
) *Usecase {
	return &Usecase{
		echoDomain: echoDomain,
	}
}
