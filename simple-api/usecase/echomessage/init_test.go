package uc_echomessage

import (
	"github.com/golang/mock/gomock"
	"reflect"
	"server/simple-api/domain/echomessage"
	"testing"
)

func TestInitUsecase(t *testing.T) {
	ctrl := gomock.NewController(t)
	echoDomainMock := echomessage.NewMockDomainItf(ctrl)
	defer ctrl.Finish()

	type args struct {
		echoDomain echomessage.DomainItf
	}
	tests := []struct {
		name string
		args args
		want *Usecase
	}{
		{
			name: "Default",
			args: args{echoDomain: echoDomainMock},
			want: &Usecase{
				echoDomain: echoDomainMock,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitUsecase(tt.args.echoDomain); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitUsecase() = %v, want %v", got, tt.want)
			}
		})
	}
}
