package uc_echomessage

import "server/simple-api/domain/echomessage"

type Usecase struct {
	echoDomain echomessage.DomainItf
}
