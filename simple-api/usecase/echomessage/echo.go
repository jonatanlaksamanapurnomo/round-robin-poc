package uc_echomessage

import (
	"context"
)

func (u *Usecase) DoEcho(ctx context.Context, data map[string]interface{}) (map[string]interface{}, error) {
	resp, err := u.echoDomain.DoEcho(ctx, data)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
