package handler

import (
	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"net/http"
	"net/http/httptest"
	"reflect"
	uc_echomessage "server/simple-api/usecase/echomessage"
	"strings"
	"testing"
)

func TestInitHandler(t *testing.T) {
	ctrl := gomock.NewController(t)
	echoUsecaseMock := uc_echomessage.NewMockEchoUsecaseItf(ctrl)
	defer ctrl.Finish()

	type args struct {
		echoUsecase uc_echomessage.EchoUsecaseItf
	}
	tests := []struct {
		name string
		args args
		want *Handler
	}{
		{
			name: "Default",
			args: args{echoUsecase: echoUsecaseMock},
			want: &Handler{echoUsecase: echoUsecaseMock},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitHandler(tt.args.echoUsecase); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitHandler() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_DoEcho(t *testing.T) {
	ctrl := gomock.NewController(t)
	echoUsecaseMock := uc_echomessage.NewMockEchoUsecaseItf(ctrl)
	defer ctrl.Finish()

	type fields struct {
		echoUsecase uc_echomessage.EchoUsecaseItf
	}
	defaultFields := fields{
		echoUsecase: echoUsecaseMock,
	}
	type args struct {
		c echo.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		mock    func()
	}{
		{
			name:   "successful echo",
			fields: defaultFields,
			args: args{
				c: func() echo.Context {
					e := echo.New()
					req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(`{"key":"value"}`))
					req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
					rec := httptest.NewRecorder()
					return e.NewContext(req, rec)
				}(),
			},
			wantErr: false,
			mock: func() {
				echoUsecaseMock.EXPECT().DoEcho(gomock.Any(), map[string]interface{}{"key": "value"}).Return(map[string]interface{}{"key": "value"}, nil)
			},
		},
	}
	for _, tt := range tests {
		tt.mock()
		t.Run(tt.name, func(t *testing.T) {
			h := &Handler{
				echoUsecase: tt.fields.echoUsecase,
			}
			if err := h.DoEcho(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("DoEcho() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
