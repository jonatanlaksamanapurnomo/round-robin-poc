package handler

import (
	"context"
	"github.com/labstack/echo/v4"
	"net/http"
	"server/simple-api/common"
	uc_echomessage "server/simple-api/usecase/echomessage"
	"time"
)

const (
	DefaultTimeout = time.Second
)

//go:generate mockgen -source=init.go -destination=init_mock.go -package=handler
type HandlerItf interface {
	DoEcho(c echo.Context) error
}
type Handler struct {
	echoUsecase uc_echomessage.EchoUsecaseItf
}

func InitHandler(
	echoUsecase uc_echomessage.EchoUsecaseItf,
) *Handler {
	return &Handler{
		echoUsecase: echoUsecase,
	}
}

func (h *Handler) DoEcho(c echo.Context) error {
	ctx, cancel := context.WithTimeout(c.Request().Context(), DefaultTimeout)
	defer cancel()

	var jsonBody map[string]interface{}
	if err := c.Bind(&jsonBody); err != nil {
		return c.JSON(http.StatusBadRequest, common.ErrorResponse(err.Error()))
	}

	responseData, err := h.echoUsecase.DoEcho(ctx, jsonBody)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, common.ErrorResponse(err.Error()))
	}

	return c.JSON(http.StatusOK, responseData)
}

func (h *Handler) Health(c echo.Context) error {
	return c.JSON(http.StatusOK, common.SuccessResponse("healthy"))
}
