package main

import (
	"flag"
	"github.com/labstack/echo/v4"
	"server/simple-api/domain/echomessage"
	"server/simple-api/handler"
	uc_echomessage "server/simple-api/usecase/echomessage"
	"time"
)

const (
	DefaultTimeout = time.Second
)

func main() {
	e := echo.New()

	echoDomain := echomessage.InitDomain()
	echoUsecase := uc_echomessage.InitUsecase(echoDomain)
	echoHandler := handler.InitHandler(echoUsecase)

	e.POST("/echo", echoHandler.DoEcho)
	e.GET("/health", echoHandler.Health)

	port := flag.String("port", "1323", "port to listen on")
	flag.Parse()
	e.Logger.Fatal(e.Start(":" + *port))
}
