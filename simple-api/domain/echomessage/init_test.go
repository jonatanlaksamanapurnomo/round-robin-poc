package echomessage

import (
	"reflect"
	"testing"
)

func TestInitDomain(t *testing.T) {
	tests := []struct {
		name string
		want *Domain
	}{
		{
			name: "initialize usecase",
			want: &Domain{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitDomain(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitDomain() = %v, want %v", got, tt.want)
			}
		})
	}
}
