package echomessage

import (
	"context"
	"reflect"
	"testing"
)

func TestDomain_DoEcho(t *testing.T) {
	type args struct {
		ctx  context.Context
		data map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]interface{}
		wantErr bool
	}{
		{
			name: "default",
			args: args{
				ctx:  context.Background(),
				data: map[string]interface{}{"key1": "value1"},
			},
			want:    map[string]interface{}{"key1": "value1"},
			wantErr: false,
		},
		{
			name: "empty data",
			args: args{
				ctx:  context.Background(),
				data: map[string]interface{}{},
			},
			want:    map[string]interface{}{},
			wantErr: false,
		},
		{
			name: "nested data",
			args: args{
				ctx: context.Background(),
				data: map[string]interface{}{
					"key1": "value1",
					"key2": map[string]interface{}{
						"nestedKey1": "nestedValue1",
					},
				},
			},
			want: map[string]interface{}{
				"key1": "value1",
				"key2": map[string]interface{}{
					"nestedKey1": "nestedValue1",
				},
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			r := &Domain{}
			got, err := r.DoEcho(tt.args.ctx, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("DoEcho() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoEcho() got = %v, want %v", got, tt.want)
			}
		})
	}
}
