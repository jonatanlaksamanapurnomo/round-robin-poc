package echomessage

import "context"

//go:generate mockgen -source=init.go -destination=init_mock.go -package=echomessage
type DomainItf interface {
	DoEcho(ctx context.Context, data map[string]interface{}) (map[string]interface{}, error)
}

func InitDomain() *Domain {
	return &Domain{}
}
