package echomessage

import "context"

func (r *Domain) DoEcho(ctx context.Context, data map[string]interface{}) (map[string]interface{}, error) {
	return data, nil
}
