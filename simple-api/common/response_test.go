package common

import (
	"reflect"
	"testing"
)

func TestSuccessResponse(t *testing.T) {
	type args struct {
		message string
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "Default",
			args: args{
				message: "success",
			},
			want: map[string]string{"status": "success"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SuccessResponse(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SuccessResponse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestErrorResponse(t *testing.T) {
	type args struct {
		message string
	}
	tests := []struct {
		name string
		args args
		want map[string]string
	}{
		{
			name: "Default",
			args: args{
				message: "error",
			},
			want: map[string]string{"error": "error"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ErrorResponse(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ErrorResponse() = %v, want %v", got, tt.want)
			}
		})
	}
}
