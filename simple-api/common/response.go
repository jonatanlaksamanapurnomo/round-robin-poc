package common

func ErrorResponse(message string) map[string]string {
	return map[string]string{"error": message}
}

func SuccessResponse(message string) map[string]string {
	return map[string]string{"status": message}
}
