.PHONY: run-api stop-api

# Define ports for API instances
PORTS := 1323 1324 1325

run-api:
	@echo "Starting API instances on ports $(PORTS)"
	@for port in $(PORTS); do \
		go run simple-api/cmd/main.go -port=$$port & \
	done

stop-api:
	@echo "Stopping API instances on ports $(PORTS)"
	@for port in $(PORTS); do \
		pkill -f "go run main.go -port=$$port"; \
	done

run-loadbalancer:
	go run loadbalancer/cmd/main.go &

stop-loadbalancer:
	@echo "Stopping Load Balancer"
	@pkill -f 'go run loadbalancer/cmd/main.go'

init:
	 go mod init server
	 go mod tidy