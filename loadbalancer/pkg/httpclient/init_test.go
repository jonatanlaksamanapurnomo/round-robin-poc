package httpclient

import (
	"net/http"
	"reflect"
	"testing"
)

func TestInitHttpClient(t *testing.T) {
	tests := []struct {
		name string
		want *Client
	}{
		{
			name: "Initialize HTTP client",
			want: &Client{httpClient: &http.Client{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitHttpClient(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitHttpClient() = %v, want %v", got, tt.want)
			}
		})
	}
}
