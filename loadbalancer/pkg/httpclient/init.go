package httpclient

import (
	"context"
	"net/http"
)

type Client struct {
	httpClient *http.Client
}

//go:generate mockgen -source=init.go -destination=init_mock.go -package=httpclient
type HttpClientItf interface {
	ForwardRequest(ctx context.Context, params RequestParams, data map[string]interface{}) (map[string]interface{}, error)
	CheckHealth(ctx context.Context, instance string) bool
}

func InitHttpClient() *Client {
	return &Client{
		httpClient: &http.Client{},
	}
}
