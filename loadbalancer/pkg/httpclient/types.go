package httpclient

type RequestParams struct {
	Instance string
	Endpoint string
	Method   string
}
