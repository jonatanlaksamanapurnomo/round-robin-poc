package httpclient

import (
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestClient_ForwardRequest(t *testing.T) {
	type fields struct {
		httpClient *http.Client
	}

	type args struct {
		ctx    context.Context
		params RequestParams
		data   map[string]interface{}
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    map[string]interface{}
		wantErr bool
		mock    func() (*httptest.Server, *http.Client)
	}{
		{
			name:   "successful request",
			fields: fields{},
			args: args{
				ctx: context.Background(),
				params: RequestParams{
					Instance: "",
					Endpoint: "/test",
					Method:   http.MethodPost,
				},
				data: map[string]interface{}{"key": "value"},
			},
			want:    map[string]interface{}{"response": "success"},
			wantErr: false,
			mock: func() (*httptest.Server, *http.Client) {
				server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
					json.NewEncoder(w).Encode(map[string]interface{}{"response": "success"})
				}))
				return server, server.Client()
			},
		},
		{
			name:   "error response from server",
			fields: fields{},
			args: args{
				ctx: context.Background(),
				params: RequestParams{
					Instance: "",
					Endpoint: "/test",
					Method:   http.MethodPost,
				},
				data: map[string]interface{}{"key": "value"},
			},
			want:    nil,
			wantErr: true,
			mock: func() (*httptest.Server, *http.Client) {
				server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
					w.Write([]byte("internal server error"))
				}))
				return server, server.Client()
			},
		},
	}

	for _, tt := range tests {
		server, client := tt.mock()
		defer server.Close()

		t.Run(tt.name, func(t *testing.T) {
			c := &Client{
				httpClient: client,
			}
			tt.args.params.Instance = server.URL

			got, err := c.ForwardRequest(tt.args.ctx, tt.args.params, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("ForwardRequest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ForwardRequest() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_CheckHealth(t *testing.T) {
	type fields struct {
		httpClient *http.Client
	}
	type args struct {
		ctx      context.Context
		instance string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
		mock   func() (*httptest.Server, *http.Client)
	}{
		{
			name:   "health check success",
			fields: fields{},
			args: args{
				ctx:      context.Background(),
				instance: "",
			},
			want: false,
			mock: func() (*httptest.Server, *http.Client) {
				server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusOK)
				}))
				return server, server.Client()
			},
		},
		{
			name:   "health check failure",
			fields: fields{},
			args: args{
				ctx:      context.Background(),
				instance: "",
			},
			want: false,
			mock: func() (*httptest.Server, *http.Client) {
				server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(http.StatusInternalServerError)
				}))
				return server, server.Client()
			},
		},
	}

	for _, tt := range tests {
		server, client := tt.mock()
		defer server.Close()

		t.Run(tt.name, func(t *testing.T) {
			c := &Client{
				httpClient: client,
			}
			tt.args.instance = server.URL

			got := c.CheckHealth(tt.args.ctx, tt.args.instance)
			if got != tt.want {
				t.Errorf("CheckHealth() got = %v, want %v", got, tt.want)
			}
		})
	}
}
