package httpclient

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func (c *Client) CheckHealth(ctx context.Context, instance string) bool {
	params := RequestParams{
		Instance: instance,
		Endpoint: "/health",
		Method:   "GET",
	}

	// No data needed for health check, pass nil
	_, err := c.ForwardRequest(ctx, params, nil)
	return err == nil
}

func (c *Client) ForwardRequest(ctx context.Context, params RequestParams, data map[string]interface{}) (map[string]interface{}, error) {
	url := fmt.Sprintf("%s%s", params.Instance, params.Endpoint)
	jsonData, err := json.Marshal(data)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequestWithContext(ctx, params.Method, url, bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		bodyBytes, _ := ioutil.ReadAll(resp.Body)
		return nil, fmt.Errorf("error response from instance: %s", string(bodyBytes))
	}

	var response map[string]interface{}
	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return nil, err
	}

	return response, nil
}
