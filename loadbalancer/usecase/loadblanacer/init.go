package loadbalancer

import (
	"context"
	"server/loadbalancer/pkg/httpclient"
	"sync"
)

type LoadBalancer struct {
	httpClient       httpclient.HttpClientItf
	healthyInstances []string
	instances        []string
	currentIndex     int
	mutex            sync.Mutex
}

//go:generate mockgen -source=init.go -destination=init_mock.go -package=loadbalancer
type LoadBalancerUsecaseItf interface {
	HandleRequest(ctx context.Context, requestParam httpclient.RequestParams, data map[string]interface{}) (map[string]interface{}, error)
	CheckHealthCheck()
}

func InitUsecase(
	httpClient httpclient.HttpClientItf,
	healthyInstances []string,
	instances []string,
	currentIndex int,
	mutex sync.Mutex,
) *LoadBalancer {
	return &LoadBalancer{
		httpClient:       httpClient,
		healthyInstances: healthyInstances,
		instances:        instances,
		currentIndex:     currentIndex,
		mutex:            mutex,
	}
}
