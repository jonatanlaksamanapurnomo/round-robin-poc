package loadbalancer

import (
	"github.com/golang/mock/gomock"
	"server/loadbalancer/pkg/httpclient"
	"sync"
	"testing"
)

func TestLoadBalancer_checkInstanceHealth(t *testing.T) {
	ctrl := gomock.NewController(t)
	httpClientMock := httpclient.NewMockHttpClientItf(ctrl)
	defer ctrl.Finish()

	type fields struct {
		httpClient       httpclient.HttpClientItf
		healthyInstances []string
		instances        []string
		currentIndex     int
		mutex            sync.Mutex
	}
	defaultFields := fields{
		httpClient: httpClientMock,
	}
	type args struct {
		instance string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
		mock   func()
	}{
		{
			name:   "Default",
			fields: defaultFields,
			want:   true,
			mock: func() {
				httpClientMock.EXPECT().CheckHealth(gomock.Any(), gomock.Any()).Return(true)
			},
		},
	}
	for _, tt := range tests {
		tt.mock()
		t.Run(tt.name, func(t *testing.T) {
			s := &LoadBalancer{
				httpClient:       tt.fields.httpClient,
				healthyInstances: tt.fields.healthyInstances,
				instances:        tt.fields.instances,
				currentIndex:     tt.fields.currentIndex,
				mutex:            tt.fields.mutex,
			}
			if got := s.checkInstanceHealth(tt.args.instance); got != tt.want {
				t.Errorf("checkInstanceHealth() = %v, want %v", got, tt.want)
			}
		})
	}
}
