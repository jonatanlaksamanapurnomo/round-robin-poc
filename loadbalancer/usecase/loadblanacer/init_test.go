package loadbalancer

import (
	"reflect"
	"server/loadbalancer/pkg/httpclient"
	"sync"
	"testing"
)

func TestInitUsecase(t *testing.T) {
	type args struct {
		httpClient       httpclient.HttpClientItf
		healthyInstances []string
		instances        []string
		currentIndex     int
		mutex            sync.Mutex
	}
	tests := []struct {
		name string
		args args
		want *LoadBalancer
	}{
		{
			name: "initialize load balancer with empty instances",
			args: args{
				httpClient:       nil,
				healthyInstances: []string{},
				instances:        []string{},
				currentIndex:     0,
				mutex:            sync.Mutex{},
			},
			want: &LoadBalancer{
				httpClient:       nil,
				healthyInstances: []string{},
				instances:        []string{},
				currentIndex:     0,
				mutex:            sync.Mutex{},
			},
		},
		{
			name: "initialize load balancer with some instances",
			args: args{
				httpClient:       nil,
				healthyInstances: []string{"instance1", "instance2"},
				instances:        []string{"instance1", "instance2", "instance3"},
				currentIndex:     1,
				mutex:            sync.Mutex{},
			},
			want: &LoadBalancer{
				httpClient:       nil,
				healthyInstances: []string{"instance1", "instance2"},
				instances:        []string{"instance1", "instance2", "instance3"},
				currentIndex:     1,
				mutex:            sync.Mutex{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := InitUsecase(tt.args.httpClient, tt.args.healthyInstances, tt.args.instances, tt.args.currentIndex, tt.args.mutex); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("InitUsecase() = %v, want %v", got, tt.want)
			}
		})
	}
}
