package loadbalancer

import "time"

const (
	HEALTHCHECK_INTERVAL = 2 * time.Second
	HEALTHCHECK_TIMEOUT  = time.Second
)
