package loadbalancer

import (
	"context"
	"fmt"
	"log"
	"server/loadbalancer/pkg/httpclient"
	"time"
)

func (s *LoadBalancer) HandleRequest(ctx context.Context, requestParam httpclient.RequestParams, data map[string]interface{}) (map[string]interface{}, error) {
	for {
		instance, err := s.getNextInstance()
		if err != nil {
			return nil, fmt.Errorf("no healthy instances available")
		}

		response, err := s.httpClient.ForwardRequest(ctx, httpclient.RequestParams{
			Instance: instance,
			Method:   requestParam.Method,
			Endpoint: requestParam.Endpoint,
		}, data)
		if err == nil {
			log.Printf("Current Instance %s", instance)
			return response, nil
		}
		log.Printf("Instance %s failed: %v. Retrying with next instance.", instance, err)
	}
}

func (s *LoadBalancer) getNextInstance() (string, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	if len(s.healthyInstances) == 0 {
		return "", fmt.Errorf("no healthy instances available")
	}
	instance := s.healthyInstances[s.currentIndex]
	s.currentIndex = (s.currentIndex + 1) % len(s.healthyInstances)
	return instance, nil
}

func (s *LoadBalancer) CheckHealthCheck() {
	for {
		s.mutex.Lock()
		var newHealthyInstances []string
		for _, instance := range s.instances {
			if s.checkInstanceHealth(instance) {
				newHealthyInstances = append(newHealthyInstances, instance)
				log.Printf("Instance %s is healthy", instance)
			} else {
				log.Printf("Instance %s is unhealthy", instance)
			}
		}
		s.healthyInstances = newHealthyInstances
		s.mutex.Unlock()
		time.Sleep(HEALTHCHECK_INTERVAL)
	}
}

func (s *LoadBalancer) checkInstanceHealth(instance string) bool {
	ctx, cancel := context.WithTimeout(context.Background(), HEALTHCHECK_TIMEOUT)
	defer cancel()
	return s.httpClient.CheckHealth(ctx, instance)
}
