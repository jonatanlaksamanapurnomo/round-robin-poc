package httphandler

import loadbalancer "server/loadbalancer/usecase/loadblanacer"

type Handler struct {
	loadBalancerUsecase loadbalancer.LoadBalancerUsecaseItf
}

func InitHttpHandler(
	loadBalancerUsecase loadbalancer.LoadBalancerUsecaseItf,
) *Handler {
	return &Handler{
		loadBalancerUsecase: loadBalancerUsecase,
	}
}
