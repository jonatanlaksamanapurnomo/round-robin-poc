package httphandler

import (
	"context"
	"github.com/labstack/echo/v4"
	"net/http"
	"server/loadbalancer/pkg/httpclient"
)

func (h *Handler) RoundRobinHandler(c echo.Context) error {
	ctx, cancel := context.WithTimeout(c.Request().Context(), DefaultTimeout)
	defer cancel()

	var jsonBody map[string]interface{}
	if err := c.Bind(&jsonBody); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": err.Error()})
	}

	response, err := h.loadBalancerUsecase.HandleRequest(ctx, httpclient.RequestParams{
		Endpoint: c.Request().URL.Path,
		Method:   c.Request().Method,
	}, jsonBody)
	if err != nil {
		return c.JSON(http.StatusServiceUnavailable, map[string]string{"error": err.Error()})
	}

	return c.JSON(http.StatusOK, response)
}
