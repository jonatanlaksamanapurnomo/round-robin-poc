package main

import (
	"github.com/labstack/echo/v4"
	"server/loadbalancer/httphandler"
	"server/loadbalancer/pkg/httpclient"
	loadbalancer "server/loadbalancer/usecase/loadblanacer"
	"sync"
)

var (
	instances = []string{
		"http://localhost:1323",
		"http://localhost:1324",
		"http://localhost:1325",
	}
	healthyInstances []string
	currentIndex     = 0
	mutex            sync.Mutex // Mutex to protect shared state
)

func main() {
	e := echo.New()

	httpClient := httpclient.InitHttpClient()
	loadbalancerUsecase := loadbalancer.InitUsecase(
		httpClient,
		healthyInstances,
		instances,
		currentIndex,
		mutex,
	)
	httpHandler := httphandler.InitHttpHandler(loadbalancerUsecase)

	go loadbalancerUsecase.CheckHealthCheck()

	e.Any("/echo", httpHandler.RoundRobinHandler)

	// Start server
	e.Logger.Fatal(e.Start(":8080"))
}
